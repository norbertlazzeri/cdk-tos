val cdkVersion = "1.83.0"
val scalaV = "2.13.4"

ThisBuild / scalaVersion := scalaV
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "hello-cdk",
    mainClass in (Compile, run) := Some("com.myorg.App"),
    libraryDependencies := Seq(
      "org.scala-lang" % "scala-library" % scalaV,
      "software.amazon.awscdk" % "core" % cdkVersion,
      "software.amazon.awscdk" % "s3" % cdkVersion,
      "software.amazon.awscdk" % "ecs-patterns" % cdkVersion,
      "software.amazon.awscdk" % "rds" % cdkVersion
    )
  )