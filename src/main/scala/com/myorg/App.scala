package com.myorg

import software.amazon.awscdk.core._
import com.myorg.basic.BasicInfra
import com.myorg.complex._

object App {
  def main(args: Array[String]): Unit = {
    val app = new App()
    new BasicInfra(app, "basic-infra-stack")
    new ComplexStack(app, "complex-stack")
    app.synth()
  }
}
