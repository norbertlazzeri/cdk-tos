package com.myorg.complex.shared

import software.amazon.awscdk.core.StackProps
import software.amazon.awscdk.core.Environment
import software.amazon.awscdk.core.Construct
import software.amazon.awscdk.services.ec2.Vpc
import software.amazon.awscdk.services.ec2.VpcLookupOptions

object QaStagingStack {
  val vpcId = "vpc-9d876ef5"
  val accountId = "929580149227"
  val regionId = "eu-central-1"

  val props = StackProps
    .builder()
    .env(
      Environment
        .builder()
        .region(regionId)
        .account(accountId)
        .build()
    )
    .build()
}

abstract class QaStagingStack(scope: Construct, id: String)
    extends EnvStack(scope, id, QaStagingStack.props) {

  val vpc = Vpc.fromLookup(
    this,
    "vpc",
    VpcLookupOptions.builder().vpcId(QaStagingStack.vpcId).build()
  )

}
