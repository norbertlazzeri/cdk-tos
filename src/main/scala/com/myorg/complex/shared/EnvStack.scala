package com.myorg.complex.shared

import software.amazon.awscdk.core.Construct

import software.amazon.awscdk.services.ec2._
import software.amazon.awscdk.core.StackProps
import software.amazon.awscdk.core.Stack

abstract class EnvStack(scope: Construct, id: String, props: StackProps)
    extends Stack(scope, id, props) {

  def vpc: IVpc

}
