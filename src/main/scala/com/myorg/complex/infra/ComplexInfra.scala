package com.myorg.complex.infra

import software.amazon.awscdk.core.Construct
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService
import software.amazon.awscdk.services.ec2.IVpc
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions
import software.amazon.awscdk.services.ecs.ContainerImage
import software.amazon.awscdk.services.ecr.assets.DockerImageAsset
import software.amazon.awscdk.services.rds.DatabaseInstance
import software.amazon.awscdk.core.CfnParameter

class ComplexInfra(scope: Construct, id: String, vpc: IVpc)
    extends Construct(scope, id) {

  val service = ApplicationLoadBalancedFargateService.Builder
    .create(this, "service")
    .cpu(512)
    .memoryLimitMiB(1024)
    .listenerPort(80)
    .vpc(vpc)
    .taskImageOptions(
      ApplicationLoadBalancedTaskImageOptions
        .builder()
        .containerPort(8000)
        .image(
          ContainerImage.fromDockerImageAsset(
            DockerImageAsset.Builder
              .create(this, "image")
              .directory("src/main/resources/docker")
              .build()
          )
        )
        .build()
    )
    .build()

}
