package com.myorg.complex

import software.amazon.awscdk.core.Construct
import com.myorg.complex.shared._
import com.myorg.complex.infra._

class ComplexStack(scope: Construct, id: String)
    extends QaStagingStack(scope, id) {

  new ComplexInfra(this, "infra", vpc)
}
