package com.myorg.basic

import software.amazon.awscdk.core._
import software.amazon.awscdk.services.s3.Bucket
import software.amazon.awscdk.services.s3.BucketEncryption
import software.amazon.awscdk.services.s3.CfnBucket

class BasicInfra(scope: Construct, id: String) extends Stack(scope, id) {

  Bucket.Builder
    .create(this, "basic-bucket-2")
    .encryption(BucketEncryption.UNENCRYPTED)
    .versioned(false)
    .removalPolicy(RemovalPolicy.DESTROY)
    .build()

}
